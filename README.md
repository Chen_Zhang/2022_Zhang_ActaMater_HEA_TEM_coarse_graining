## 2022_Zhang-et-al__ActaMater__data-mining-of-in-situ-TEM-experiments

This folder contains:

(1) The original TEM video and the stabilized video analyzed in the paper.

(2) The data needed to generate the images in the paper and the corresponding code (plot_figure.ipynb).

(3) The code for the dislocation 3D reconstruction and a discussion of the conventions for using Euler angles, etc.